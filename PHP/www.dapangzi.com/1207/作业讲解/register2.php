<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <script src="js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/bootstrap.js" type="text/javascript" charset="utf-8"></script>

    <title>注册页面</title>
    <link type="text/css" rel="stylesheet" href="style.css"/>

</head>
<body>
<div class="xm-logo">
    <i>
        <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <rect width="48" height="48" rx="3" ry="3" style="fill:#551fff"></rect>
            <rect x="10" y="15" width="4.2" height="18" style="fill:#FFFFFF"></rect>
            <rect x="33.75" y="15" width="4.2" height="18" style="fill:#FFFFFF"></rect>
            <rect x="11.25" y="15" width="15" height="4.1" style="fill:#FFFFFF"></rect>
            <rect x="26.2" y="20.55" width="4.2" height="12.5" style="fill:#FFFFFF"></rect>
            <rect x="20.6" y="15" width="9.8" height="9.8" rx="4.4" ry="4.4" style="fill:#FFFFFF"></rect>
            <rect x="20.2" y="19" width="6" height="6" rx="2" ry="2" style="fill:#551fff"></rect>
            <rect x="18" y="19.1" width="4" height="2.2" style="fill:#551fff"></rect>
            <rect x="24.2" y="23" width="1.94" height="10" style="fill:#551fff"></rect>
            <rect x="18.1" y="22.8" width="4.2" height="10.2" style="fill:#FFFFFF"></rect>
        </svg>
    </i>
</div>
<h1 class="xm-title">请填写验证码</h1>
<form class="">
    <ul class="xm-login">
        <li>
            <input class="xm-input" readonly type="text" placeholder="请输入手机号码" value="<?php echo $_GET['phone']?>"/>
        </li>
        <li>
            <input class="xm-input" type="text" placeholder="请输入验证码"/>
        </li>
        <li>
            <button class="xm-button">注册</button>
        </li>
    </ul>

</form>

</body>
</html>
