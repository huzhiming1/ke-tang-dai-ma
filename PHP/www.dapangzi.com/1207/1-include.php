<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-10
 * Time: 09:51
 */

//include "4-uploads.php";

//include 包含、包括  【报警告】
/*
echo 1;
//include "4-uploads22341234.php";

include_once "test.php";


echo 2;
//require 要求；需要；命令 【会报错】
require "test.php";
//require "test.php";//重复引入

require_once "test.php";
require_once "test.php";
require_once "test.php";
require_once "test.php";
require_once "test.php";

echo 3;*/

/*

include : 包含文件，若文件不存在会警告
include_once 出现重复引入，只会引入一次（确保唯一）
require : 需要文件，若文件不存在会报错
require_once 出现重复引入，只会引入一次（确保唯一）

*/

require_once "../1206(文件)/4-uploads.php";
$file = $_FILES['file1'];
$imgs = upload($file);
var_dump($imgs);

