<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-05
 * Time: 11:35
 */

//-----------------------1、strlen 获取字符串的长度（对中文不友好） --------------------------
echo "<hr/>";
$str = "hello";
echo strlen($str)."<br>";//5

$str = "hello你好";
echo strlen($str)."<br>";//11 中文3个字符

//-----------------------2、iconv_strlen 支持中英混合判断长度--------------------------
echo iconv_strlen($str)."<br>";//7个 

//-----------------------3、mb_strlen 支持中英文混合，但是注意字符编码--------------------------
echo mb_strlen($str);//7个


//-----------------------4、substr 截取字符，对中文不友好--------------------------
echo "<hr>";

$str4 = "abcde大家好，今天是星期四";
echo substr($str4,0,4);//abcd
echo substr($str4,5,9);//大

//-----------------------5、mb_substr 截取字符串，支持中文，注意编码--------------------------
echo "<br> mb_substr <br>";
$str4 = "abcde大家好，今天是星期四";
echo mb_substr($str4,0,4);//abcd
echo mb_substr($str4,5,9);//大家好，今天是星期

//-----------------------6、iconv_substr 截取字符串，支持中文--------------------------
echo "<br> iconv_substr <br>";
$str4 = "abcde大家好，今天是星期四";
echo iconv_substr($str4,0,4);//abcd
echo iconv_substr($str4,5,9);//大家好，今天是星期

//-----------------------7、trim 去指左右两边指定的字符--------------------------
echo "<hr/>";
$str5 = "Hello World             ";
echo trim($str5)."<br />";//Hello World
//后面的空格保留
echo trim($str5,"H")."<br />";//ello World

dump(trim($str5,"H"));

//-----------------------7.1  ltrim 去掉左边 rtrim去掉右边  --------------------------



//-----------------------8. strstr 字符串查找 返回该字符串及后面剩余部分--------------------------
echo "<hr>";
$str8 = "abdekfjakldj";
dump($str8);
dump(strstr($str8,"f"));

//-----------------------8.1. strpos 字符串查找 返回找到的下标位置，找不到返回false --------------------------
echo "<hr>";
$str81 = "abdekfjakldj";
dump($str81);
dump(strpos($str81,"f"));


//-----------------------9.字符串替换(常用)--------------------------

$str9 = "abdekfjakldj";
$str9_res = str_replace("a","b",$str9);
echo $str9_res;//bbdekfjbkldj


//-----------------------10.重复执行--------------------------
echo str_repeat("我爱学习！",10);


//-----------------------11. 大写转小写--------------------------
$str11 = "asdfEEKJLlksdjasf";
dump( strtolower($str11) );


//-----------------------12. 字符串加密 MD5不可逆性 --------------------------
echo md5("123456");//MD5文件校验、存放密码


//-----------------------13.将html标签转为实体--------------------------
$str13 = "<h1>这是一个段落标签</h1>";
$str13 = htmlspecialchars($str13);
echo $str13;

//-----------------------14.urlencode()把url编码加密--------------------------

$url = "https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=baidu&wd=天气";
echo urlencode($url);

$url2 = "https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=baidu&wd=%E5%A4%A9%E6%B0%94";
echo urldecode($url2);

//-----------------------15.explode 字符串转成数组   --------------------------
// 1,2,3,4 -> [1,2,3,4]
$str15 = "1,2,3,4";// -> [1,2,3,4]
$str15_res = explode(",",$str15);
dump( $str15_res );

//-----------------------16.implode 数组转成字符串--------------------------
// [1,2,3,4] -> "1,2,3,4"
$str16 = [1,2,3,4];
$str16_res = implode("|",$str16);
dump( $str16_res );

/**
 * 输出内容函数
 * @param $str
 */
function dump($str){
    echo "<p>";
    var_dump($str);
    echo "</p>";
    echo "<br/>";
}