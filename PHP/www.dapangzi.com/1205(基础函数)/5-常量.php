<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-05
 * Time: 11:11
 */
//-----------------------1、系统常量--------------------------
echo "<hr />";
echo PHP_OS;//mac Darwin win:WINNT

// 由于语法的升级改进，需要进行版本兼容判断
echo PHP_VERSION;//7.1.1


//-----------------------2、魔术常量--------------------------
echo "<hr />";
//2.1 获取行号（常用于报错）
echo __LINE__;echo "<br />";


//2.2 获取物理文件目录（常用于上传文件、图片）
echo __DIR__;echo "<br />";

//2.3 获取文件
echo __FILE__;echo "<br />";

//2.4 获取函数名称
function add(){
    echo __FUNCTION__;//add
}
add();

//-----------------------3、自定义常量--------------------------
echo "<hr />";

define("DAPANGZI","大胖子");

//无法被重新赋值
//DAPANGZI = "小胖子";
echo DAPANGZI;
//判断是否设置过某个常量
$s = defined("DAPANGZI");
var_dump($s);

//用自定义常量可以在全系统中进行访问，它是不能再被改变。


//System.out.println("%@%d%f","大家好",19,10.00);

