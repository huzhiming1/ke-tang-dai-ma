<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-05
 * Time: 16:02
 */

$cars = array("宝马", "沃尔沃", "", "本田", "大众");

//-----------------------1、数组 key 指针的位置操作--------------------------

//echo key($cars);//0
//echo current($cars);//宝马
//echo next($cars);//沃尔沃
//echo next($cars);//本田
//echo next($cars);//大众
if ("沃尔沃" == "大众") {
    echo "1";
} else {
    echo "2";
}


$end = end($cars);//大众
reset($cars);//重置到第一个
//使用while循环移动的方式输出所有数组的值
while (true) {
    echo current($cars);
    next($cars);
    if (current($cars) == $end) {
        echo current($cars);
        break;
    }
}

echo "<hr>";
$cars1 = array("宝马1", "沃尔沃1", "", "本田1", "大众1");
while (next($cars1)) {
    echo current($cars1) . ",";
}


//-----------------------2、数组基本操作--------------------------

$car2 = array("宝马1", "沃尔沃1", "", "本田1", "大众1");

array_unshift($car2, "丰田");
dump($car2);

array_shift($car2);//移除第一个
dump($car2);


array_pop($car2);//移除最后一个
dump($car2);


//-----------------------3.合并数组--------------------------
$car3 = array("拖拉机", "柴油机");
$car4 = array_merge($car2, $car3);//合并数组
dump($car4);


//-----------------------4.//打乱数组 , 打斗地主，洗牌--------------------------
shuffle($car4);
dump($car4);


//-----------------------5. 排序 sort rsort倒序--------------------------
$arr5 = ['d', "c", "b", 'a'];
sort($arr5);//
dump($arr5);


//-----------------------6.排序 ksort krsort倒序--------------------------
$arr6 = [
    'user' => "张三",
    "age" => 18,
    "name" => "老张"
];
ksort($arr6);//做 参数排序 密码 key
dump($arr6);



//-----------------------7. 分割数组,第二个参数是分割数量--------------------------
$car7 = array("宝马1", "沃尔沃1", "", "本田1", "大众1");
dump(array_chunk($car7,2));


/**
 * 输出内容函数
 * @param $str
 */
function dump($str)
{
    echo "<p>";
    var_dump($str);
    echo "</p>";
    echo "<br/>";
}

?>