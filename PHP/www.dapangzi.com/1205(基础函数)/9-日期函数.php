<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-05
 * Time: 16:06
 */

//-----------------------时间戳--------------------------
echo time() + 60;//1970年01月01日到今天的秒数
//时间戳的作用就是为了方便  增 减

echo "<br>";


//-----------------------日期和时间--------------------------
// Y = 4个数字的年份
// m 月份
// d 天

// H 小时
// i 分钟
// s 秒
echo  date("Y-m-d H:i:s")."<br>";

echo  date("Y年m月d日 H:i:s")."<br>";

$str = "2019-12-05 16:12:58";
//标准时间格式，转为时间戳
echo strtotime($str)."<br>";

//时间戳转为标准时间格式
echo date("Y-m-d H:i:s","1575533578")."<br>";