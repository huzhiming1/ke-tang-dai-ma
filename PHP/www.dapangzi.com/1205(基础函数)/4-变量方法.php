<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-05
 * Time: 10:58
 */


//从post里面取值

//-----------------------1、isset 是否设置--------------------------
if (isset($_POST['user'])){
    $user = $_POST['user'];//关联数组取值
}else{
    $user = "";
}
$pass = isset($_POST['pass'])? $_POST['pass'] : "";//关联数组取值


//-----------------------2、empty 判断为空--------------------------
if (empty($user)){
//    echo "用户名是空的";
}
echo $user;


//-----------------------3、unset 删除变量--------------------------
//删除user 变量
unset($user);
//echo $user;//这里会报错，因为已经被删除了变量

$_POST['user'] = "张三";
$_POST['age'] = 18;


//-----------------------4、unset $arr[] 删除数组中指定变量--------------------------
var_dump($_POST);
unset($_POST['age']);//删除数组中指定元素
var_dump($_POST);//age被删除
