<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 16:38
 */

class Person2{

    //姓名  公共的
    public  $name;
    //性别  被保护的
    protected $gender;
    //年龄  私有的
    private $age;

    //余额
    private $yue;

    public function __construct()
    {
        $this->name = "1";
        $this->gender = "男";
        //私有属性，只有自己可以调用
        $this->age = 18;
        $this->yue = 200;

    }

    /**
     * 跳
     */
    public function jump(){
        echo "<br/>".$this->name."跳1米<br/>";
    }

    /**
     * 花钱
     */
    protected function huaqian(int $money){
        echo "父类花钱";
        $this->yue -= $money;
    }

}

class Student2 extends Person2{
    public $yue;

    //重写
    public function __construct()
    {
        echo $this->name;
        $this->gender = "男";
        echo $this->gender;
        $this->yue = 50;
        //私有属性，只有自己可以调用
//        echo $this->age;
    }

    //重写
    public function jump()
    {
        echo "<br/>".$this->name."跳2米<br/>";
    }

    public function huaqian(int $money)
    {
        //如果花费超过100元，就让父类付款
        if ($money>100){
            //调用父类的方法用parent
            parent::huaqian($money);
        }else{
            echo "子类花钱";
            $this->yue -= $money;
        }

    }
}

$stu = new Student2();
$stu->name = "小王";
$stu->jump();//2米

echo $stu->name;

$stu->huaqian(99);

//因为是私有的，无法操作
//$stu->age = 18;
//$stu->gender = "男";


$xiaohua = new Person2();
$xiaohua->name = "小花";
$xiaohua->jump();//1米
