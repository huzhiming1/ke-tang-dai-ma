<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 09:10
 */

/**
 * Class User
 * 类使用class定义
 * 命名规范：首字母大写，多个单词一般使用驼峰，取名有意义。
 *
 */
class User{

    //定义[属性],理解为变量
    //多了一种修饰符：公共的、私有的、被保护的、静态的
    private $account;//账户
    protected $phone;//手机号
    private $code;//验证码
    public $nickname;//昵称
    private $idcard;//身份证
    private $password;//密码
    private $yue;//银行卡余额
    private $age;//年龄
    private $c_time;//创建时间
    private $c_ip;//创建IP地址
    private $u_time;//修改时间

    /**
     * 什么叫方法：函数的另一个名称
     * 只是被限制了自由，只能在类里面。
     * 输出用户信息
     * @param $account
     */
    public function userinfo(){


        //操作属性
        echo "账户名:".$this->account.",年龄：".$this->age;
    }


    /**
     * 可以将set方法理解为安检通道，只有经过合法性判断才允许设置值。
     */

    /**
     * 设置账户值 a123456
     * @param mixed $account
     */


    public function setAccount($account): void
    {
        //一系列的账号合法性判断
        $this->account = $account;
    }

    /**
     * 获取账户值
     * @return mixed
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    public function setYue($yue){
        //一系列的数据值合法性判断
        if($yue>=0){
            $this->yue = $yue;
        }
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        if ($age > 0){
            $this->age = $age;
        }else{
            $this->age = 0;
        }
    }


}//类的作用域结束

//创建对象 关键词 new；对象来自于类

//-----------------------标准创建对象--------------------------
$user1 = new User();

//-----------------------操作属性--------------------------
$user1->nickname = "张三";

//-----------------------操作方法--------------------------
$user1->setAccount("a123456");
$user1->setAge(-100);
$user1->userinfo();


$user2 = new User();
echo $user2->nickname;//null

//-----------------------变量创建--------------------------
$a = "User";
$user = new $a();


//-----------------------类内部使用 new self();--------------------------
//new self();
var_dump($user);

$p = new Person();//创建对象
echo $p->gender;//性别

//-----------------------静态方法创建对象--------------------------
//静态方法创建; （对于数据库，已打开过的，不需再次打开。单例设计模式）
$p1 = Person::getPerson();
echo "静态方法创建对象";
var_dump($p1);

echo Person::NAME;//打印常量

$xiaoming = new Person();
$xiaoming -> run();


//一个文件一般是一个类
class Person{

    const NAME = "张三";//定义常量
    public $height;
    public $weight;
    public $gender;//性别
    private $money;
    protected $age;


    //静态方法属于类的
    public  static  function getPerson(){
        $p = new self();
        $p->gender = "男";
        return $p;//创建了对象
    }

    //普通方法是属于对象的
    /**
     * 跑
     */
    public function run(){
        echo "跑";
    }

}




