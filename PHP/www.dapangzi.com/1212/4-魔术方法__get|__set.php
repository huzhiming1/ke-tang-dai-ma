<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 16:03
 */


class Student{
    private $stuno;
    private $class;

    /**
     * @param mixed $class
     */
//    public function setClass($class): void
//    {
//        $this->class = $class;
//    }
//
//    /**
//     * @param mixed $stuno
//     */
//    public function setStuno($stuno): void
//    {
//        $this->stuno = $stuno;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getClass()
//    {
//        return $this->class;
//    }

    /**
     * @return mixed
     */
    public function get($a)
    {
        return $this->$a;
    }

    /**
     * @return mixed
     */
    public function set($name,$value)
    {
         $this->$name = $value;
    }


    /**
     * 魔术方法
     * @param $name 属性名
     * @param $value 属性值
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }


}


$stu = new Student();
//$stu->setStuno("888999");
//$stu->setClass("10班");
//$stu->set("class","10班");
//echo  $stu->get("class");
//因为有set方法，才能直接操作属性
$stu->class = "10班";
//因为有get魔术方法，才能获取值
echo $stu->class;
