<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 11:10
 */

class Person
{
    private $age;
    public $name;
    public $gongzuo;
    public $xueli;
    public $yue;
    //构造方法（被创建时触发）
    public function __construct($name,$age)
    {
        $this->name = $name;//取名
        $this->age = $age;
        //连接数据库
        //打开文件fopen

        echo "<br/>对象实例化了<br/>";

    }


    //析构方法（被销毁时触发）
    public function __destruct()
    {
        //释放人的一些权力
        //释放内存
        //关闭数据库
        //fclose
        echo "<br/>{$this->name}对象被销毁了<br/>";
    }

     function xiyifu(){
        echo $this->name."在洗衣服";
     }
     function zuofan(){
        echo $this->name."在做饭";
     }

    function changge(){
        echo $this->name."在唱歌";
    }

    function chifan(){
        echo $this->name."在吃饭";
    }
    function shuijiao(){
        echo $this->name."在睡觉";
    }
    function gouwu(){
        echo $this->name."在购物";
    }

    /**
     * 成婚方法
     *
     * @param Person $person 结婚对象是谁
     * @param $money 结婚的消费
     */
    function chenghun(Person $person,int $money){
        if ($this->yue>=$money){
            $this->yue -= $money;
            echo "<br/>".$this->name."跟".$person->name."成婚了~，共消费".$money;
        }else{
            echo "<br/>".$this->name."余额不足，无法成婚";
        }
    }

    function info(){
        echo "我叫".$this->name."，年龄".$this->age."，学历".$this->xueli."，我".($this->gongzuo?"有":"无")."工作";
    }


}