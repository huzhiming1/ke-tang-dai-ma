<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 14:48
 */

include_once "Person.php";

//对象的销毁，取决于引用是否为0


//值的引用（牵引绳）
$xh1 = new Person("小黑",3);//1 2
$xh2 = $xh1;//引用
$xh2->name="大黑";
echo $xh1->name;//小黑

//克隆对象的问题clone
$xh3 = clone $xh1;//克隆 和 普通数据 赋值
$xh3->name="大黑3";
echo $xh1->name;//小黑
echo $xh3->name;//大黑3


//如何销毁变量?
$a = 10;
//echo  $a;//10
//echo $a;
//销毁
unset($a);
//echo $a;


//如何销毁对象?
unset($xc1);






