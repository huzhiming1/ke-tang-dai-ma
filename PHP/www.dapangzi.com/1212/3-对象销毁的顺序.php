<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 14:48
 */

include_once "Person.php";

//对象的销毁，取决于引用是否为0

$p1 = new Person("P1",3);
$p4 =  $p1;
unset($p1);//p1销毁了

$p2 = new Person("P2",3);
unset($p4);//第二个引用

$p3 = new Person("P3",3);
//p1
//p3
//p2

echo "程序结束了<br>";
