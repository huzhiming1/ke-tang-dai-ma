<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-12
 * Time: 11:12
 */

include_once "Person.php";

//new Person();




//练习一：
//定义一个"人"类；
//实例化一个姓名为小翠，芳龄22；
//她会洗衣服、会做饭、无工作；大专学历。
//自我介绍
$xiaocui = new Person("小翠",22);
//$xiaocui->name = "小翠";
//$xiaocui->age = 22;
$xiaocui->gongzuo = false;
$xiaocui->xueli = "大专";
$xiaocui->info();



//练习二：
//再实例化一个姓名为yoyo，芳龄18；
//她会购物、唱歌、有工作；高中学历。
//自我介绍
$yoyo = new Person("yoyo",18);
//$yoyo->name = "yoyo";
//$yoyo->age = 18;
$yoyo->gongzuo = true;
$yoyo->xueli = "高中";
$yoyo->info();
$yoyo->gouwu();



//练习三：
//在实例化一个你，然后姓名：高富帅、年龄19、存款；
//有工作、吃饭、睡觉、成婚（选择一个练习一和二 中的某个对象）
//成婚需要花费100元
//让你成婚，再信息介绍：这个人年龄、余额、对象是谁。
//
$gaofushuai = new Person("高富帅",19);
//$gaofushuai->name = "高富帅";
$gaofushuai->xueli = "无";
//$gaofushuai->age = 19;无法操作私有属性
$gaofushuai->yue = 200;
$gaofushuai->gongzuo = true;
$gaofushuai->info();

echo "<br/>高富帅的余额".$gaofushuai->yue."<br>";
$gaofushuai->chenghun($xiaocui,80);
$gaofushuai->chenghun($yoyo,150);
echo "<br/>结婚后高富帅的余额".$gaofushuai->yue."<br>";





