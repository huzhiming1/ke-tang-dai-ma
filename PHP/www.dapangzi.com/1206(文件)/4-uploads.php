<?php
/**
 * 多图上传
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-06
 * Time: 11:27
 */

//$files = $_FILES['file1'];//从页面获取文件名称
//
//$imags = upload($files);//调用上传方法
//
//var_dump($imags);

//ok 1、存放的位置（年月日文件夹）
//ok 2、图片的格式（获取后缀.jpg|.png|.gif）
//ok 3、图片的名称（年月日时分秒+毫秒+随机一个名称，要保证整个系统不能重复）
//      uniqid();//随机一个名称
//   4、限制上传的图片类型(JPG|PNG|GIF)
//ok    4.1 一种后端判断上传的文件类型
//ok    4.2 一种在前端只允许上传图片类型accept="image/gif,image/jpeg,image/jpg,image/png"
//ok 5、限制上传文件的大小（2M以内）
//ok 6、多图上传
//0k 7、预览图片上传后的图片得显示出来

/**
 * 图片单图或多图上传
 * @param $files
 * @return array
 */
function upload($files){

    $image_paths = array();//最终图片存放数组
    //多图上传
    //判断取到的图片是否为数组，是数组代表多图上传
    if (is_array($files['name']) == true){
        $count = count($files['name']);//获取名称得到图片个数
        for ($i = 0;$i<$count;$i++){
            $tmp_file = $files['tmp_name'][$i];//图片的临时路径
            $name = date("ymdHis").uniqid().$files['name'][$i];//图片的名称
            $type = $files['type'][$i];//图片的类型
            $size = $files['size'][$i];//图片的大小
            $imgpath = move($tmp_file,$name,$type,$size);
            array_push($image_paths,$imgpath);
        }
    }else{
        $file = $files;
        $tmp_file = $file['tmp_name'];//图片的临时路径
        $name = date("ymdHis").uniqid().$file['name'];//图片的名称
        $type = $file['type'];//图片的类型
        $size = $file['size'];//图片的大小
        $imgpath = move($tmp_file,$name,$type,$size);
        array_push($image_paths,$imgpath);
    }
    return $image_paths;
}


/**
 *
 * 移动临时图片
 * @param $tmp_file
 * @param $name
 * @param $type
 * @param $size
 * @return string
 */
function move($tmp_file,$name,$type,$size){

//image/png  | image/jpeg | image/webp | image/gif | image/x-icon
    $image_types = ["image/png",'image/jpeg',"image/webp","image/gif","image/x-icon"];

    if (!in_array($type,$image_types)){
        echo "仅支持以下图片类型：".implode(",",$image_types);
        die();//结束
    }
    if ($size > 2048*1024){
        echo "您上传的文件太大了";
        die();//结束
    }
    $dirname = date("Y/m/d");//2019/12/06
    if(!is_dir($dirname)){
        mkdir($dirname,0777,true);
    }
    move_uploaded_file($tmp_file,"$dirname/{$name}");
    return "$dirname/$name";
}

