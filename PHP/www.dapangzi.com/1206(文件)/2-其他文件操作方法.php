<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-06
 * Time: 10:04
 */

//-----------------------偶尔用一些文件操作，用它--------------------------

//-----------------------读取文件--------------------------
echo file_get_contents("abc.txt");



//-----------------------写入文件（性能很低）--------------------------
$content = "10月26日上午7时左右，在福建莆田南日岛附近海域，一艘渔船因船体破损进水发生侧翻，福建海警接到报警电话后紧急出动。经过30分钟紧急救援，成功帮助11名船员脱险。";
file_put_contents("abc.txt",$content);


//-----------------------频繁的写入(性能很低)--------------------------
//echo "开始";
//$start = time();
//for($a = 0;$a < 10000;$a++){
//    file_put_contents("abc.txt",$content);
//}
//$end = time();
//echo "总耗时".($end-$start);


//-----------------------使用fopen优化写入性能--------------------------
echo "开始2";
echo "<br>";
$start = time();
//echo ">>".$start."<br>";
echo ">>".microtime(true)."<br>";
$fc = fopen("book.txt","w+");

for($a = 0;$a < 10000;$a++){
   fwrite($fc,"饕");
}

fclose($fc);

$end = time();
echo "总耗时".($end-$start);