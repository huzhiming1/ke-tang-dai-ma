<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-06
 * Time: 11:27
 */

$file = $_FILES['file1'];

$path = upload($file);//调用上传函数方法

//打印 | 存入数据库 | ...
//echo $path;
//savesql($path);





function upload($file){


    $tmp_file = $file['tmp_name'];//图片的临时路径
    $name = date("ymdHis").uniqid().$file['name'];//图片的名称
    $type = $file['type'];//图片的类型
    $size = $file['size'];//图片的大小


//image/png  | image/jpeg | image/webp | image/gif | image/x-icon
    $image_types = ["image/png",'image/jpeg',"image/webp","image/gif","image/x-icon"];

    if (!in_array($type,$image_types)){
        echo "仅支持以下图片类型：".implode(",",$image_types);
        die();//结束
    }
    if ($size > 2048*1024){
        echo "您上传的文件太大了";
        die();//结束
    }
    $dirname = date("Y/m/d");//2019-12-06-H
    if(!is_dir($dirname)){
        mkdir($dirname,0777,true);
    }
    move_uploaded_file($tmp_file,"$dirname/{$name}");


    return "$dirname/$name";
}
//ok 1、存放的位置（年月日文件夹）
//ok 2、图片的格式（获取后缀.jpg|.png|.gif）
//ok 3、图片的名称（年月日时分秒+毫秒+随机一个名称，要保证整个系统不能重复）
//      uniqid();//随机一个名称
//   4、限制上传的图片类型(JPG|PNG|GIF)
//ok    4.1 一种后端判断上传的文件类型
//ok    4.2 一种在前端只允许上传图片类型accept="image/gif,image/jpeg,image/jpg,image/png"
//ok 5、限制上传文件的大小（2M以内）
//   6、多图上传
//0k 7、预览图片上传后的图片得显示出来,拿到图片的路径
