<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-06
 * Time: 09:12
 */
define("DEBUG",true);//调试模式

if (DEBUG){
    error_reporting(E_ALL);
}else{
    error_reporting(0);//关闭错误信息
}


//-----------------------1、打开文件--------------------------
//$fc = fopen("a.txt","r") or die("文件打开异常");
$fc = fopen("abc.txt","a+") or die("文件打开异常");
var_dump($fc);//resource

//-----------------------2、读取文件--------------------------
$str = fread($fc,2);
echo "读取内容：".$str;

//-----------------------3、写内容入文件--------------------------
//fwrite($fc,"你好~");

//-----------------------4、指针读取文件到末尾--------------------------
$str .= fread($fc,10);
var_dump($str);

$str .= fread($fc,10);
var_dump($str);

$str .= fread($fc,10);
var_dump($str);

if(feof($fc)){
    echo "文件到末尾了";
}else{
    echo "文件未到末尾";
}

echo "<hr/>";
$fc2 = fopen("a.txt","a+") or die("文件打开异常");
while(!feof($fc2)){
    echo fread($fc2,5);
}



//-----------------------5、关闭文件（好习惯）--------------------------
fclose($fc);
fclose($fc2);

//echo fread($fc,"2");

//-----------------------6、删除文件--------------------------
unlink("a.txt");





$fc2 = fopen("a.txt","a+") or die("文件打开异常");
fwrite($fc2,"aa");
fwrite($fc2,"aa");
fwrite($fc2,"aa");
fwrite($fc2,"aa");
fclose($fc2);
