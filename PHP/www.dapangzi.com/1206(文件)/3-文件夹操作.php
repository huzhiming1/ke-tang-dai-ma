<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-06
 * Time: 10:34
 */


//-----------------------文件夹操作--------------------------
$dirname = "./a/b";

//-----------------------is_dir 判断文件夹是否已存在--------------------------
//-----------------------mkdir 创建文件夹--------------------------

//如果文件不存在
if (!is_dir($dirname)){
    //创建文件
    mkdir($dirname);
}

//创建文件"a.txt" 到指定的目录"./a/b/a.txt"
$filename = $dirname."a.txt";
fopen($filename,'a+');


//mkdir("./c/d",0777,true);

//练习： 以年月日创建文件夹，例如今天是"2019/12/06"。
$name = date("Y/m/d");//2019/12/06
mkdir($name,0777,true);










