<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-11
 * Time: 09:28
 */

/*
    二、登录功能 思路：
前端部分：
    1、页面定义两个输入框（用户名、密码）input
    2、页面form表单：action指向后端~登录的~PHP文件，注意提交类型POST。action method
    3、页面定义提交按钮，触发表单提交事件 submit
后端部分：
    1、使用POST提交页面传递的用户名、密码 $_POST
    2、从文件中读取用户数据
        2.1 读取data.json中的数据 file_get_contents
        2.2 解码decode 为关联数组，注意这里是读取的全部数据 json_decode ($json,true)
    3、从全部数据中循环获取到登录用户的数据 foreach
    4、判断账号密码是否正确 if
    5、登录成功|登录失败
*/

$phone = $_POST['phone'] ;

if (!preg_match("/0?(13|14|15|17|18|19)[0-9]{9}/",$phone)){
    echo "手机格式错误";
    die();
}
$pass = $_POST['password'];

if (strlen($pass) < 6){
    echo "密码格式错误";
    die();
}


//key=>value
$new_user = array("phone"=>$phone,"pass"=>$pass);

//从json读取数据
$data = file_get_contents("data.json");

//这是用户数组
$users = $data == '' ? array() : json_decode($data,true);

//判断用户是否存在
foreach ($users as $user){
    if ($phone ==  $user['phone']){
        //判断密码是否一致
        if($pass == $user['pass'])
        {
            echo "登录成功 <a href='list.php'>查看用户列表</a>";
            die();
        }else{
            echo "密码错误";
            die();
        }
    }
}
echo "登录失败,不存在该用户";
die();
