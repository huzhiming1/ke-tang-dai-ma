<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-11
 * Time: 09:28
 */

/*
    1、使用POST接收页面传递的参数（用户名、密码）$_POST
    2、将用户名与密码进行组合为关联数组（key=>value）
    3、文件读取
        3.1 读取data.json中已存放的用户数据 file_get_contents
        3.2 解码(decode)JSON数据为关联数组 json_decode ($json,true)
    4、用户增加至关联数组
        4.1 往关联数组中加入一条记录（新用户）array_push
    5、保存新数据至文件
        5.1 清空data.json数据内容  file_put_contents
        5.2 将关联数组编码encode为JSON数据 json_encode
        5.3 将JSON数据写入至data.json文件中 file_put_contents
    6、注册成功
*/

$phone = $_POST['phone'] ;

if (!preg_match("/0?(13|14|15|17|18|19)[0-9]{9}/",$phone)){
    echo "手机格式错误";
    die();
}
$pass = $_POST['password'];

if (strlen($pass) < 6){
    echo "密码格式错误";
    die();
}


//key=>value
$new_user = array("phone"=>$phone,"pass"=>$pass);

//从json读取数据
$data = file_get_contents("data.json");

//这是用户数组
$users = $data == '' ? array() : json_decode($data,true);

//判断用户是否存在
foreach ($users as $user){
    if ($phone ==  $user['phone']){
        echo "用户已存在";
        die();
    }
}

//第一次用户注册时，会导致无内容，添加失败
array_push($users,$new_user);

//清空
file_put_contents("data.json","");
//写入
$res = file_put_contents("data.json",json_encode($users));
if ($res){
    echo "注册成功~";
}else{
    echo "注册失败";
}