<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-04
 * Time: 15:09
 */


//
//完成输出1~100，包含的7有关的数字就跳过。
//请使用break 或 continue

//while
$i = 0;
while($i < 10){
    $i++;
    echo $i;//1 2 3 4
}

echo "<hr>";

$j = 0;
while ($j <= 100){
    $j++;
    $str = $j."";//为了用字符串函数，转成字符串
    $is = strstr($str,"7");
    if ($is){
        continue;
    }
    echo $str."<br>";
}

echo "<hr/>";

$i = 0;
//正则表达式的写法
while ($i <= 100) {
//    echo preg_match("/7/",$i);
    if (preg_match("/7/",$i)!=1){
        echo "<br>", $i;
    }
    $i++;
}








