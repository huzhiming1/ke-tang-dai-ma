<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-04
 * Time: 14:09
 */

    $a =  1;//整型
    $b = "Hello";//字符串
    $c = true;//布尔类型
    $d = 3.14;//浮点类型

    echo $b;//打印 标量基础数据类型
//    print ;
//    echo "<br><h1>大标题</h1><script type='text/javascript'>alert('1')</script>";
    var_dump($b);//推荐打印方式，支持普通数据类型、还支持复杂的对象、数组、键值对

//    索引数组
    echo "<hr><h1>索引数组</h1>";
    $arr = ["a","b","c"];//简写数组
    $arr2 = array("a","b","c");
    echo $arr[0];//a
    echo $arr;
    echo "<br><pre>";
    var_dump($arr);

//    关联数组
echo "<hr><h1>关联数组</h1> <h2>";
    $keyArray = [
        "user"=>"zhangsan",
        "pass"=>"1234567"
    ];
    var_dump($keyArray);


    echo phpinfo();



