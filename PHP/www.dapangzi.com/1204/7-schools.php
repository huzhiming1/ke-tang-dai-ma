<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-04
 * Time: 17:01
 */

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,OPTIONS,GET ");

if(empty($_GET['word'])){

    $data['code'] = -1;
    $data['message'] = "请输入关键词word";
//    $data = ["code"=> -1,"message"=>"请输入关键词word"];
    echo json_encode($data);
    exit();
}

$key = $_GET['word'];//GET POST

//文件 _ 获取 _ 内容
$json = file_get_contents("university.json");

//解码json
$arr = json_decode($json);

$univers = array();//所有院校

foreach ($arr as $provinces){
    $cities  = $provinces -> cities;//取出城市列表

    foreach ($cities as $citys){
        $item = $citys->universities;//取出每个城市的大学列表

        //将每个城市的大学都加入到大数组中;

        $univers = array_merge($univers,$item);
//        foreach ($item as $k=>$v){
//           $universities[] = $v;//
//        }
    }
}

//通过关键词匹配新数组
$res = [];
foreach ($univers as $school){

    if (count($res) > 10) break;
    if (strstr($school,$key)){
        $res[] = $school;
    }

}
$data['code'] = 1;
$data['message'] = "匹配成功";
$data['data'] = $res;
echo json_encode($data,JSON_UNESCAPED_UNICODE);

