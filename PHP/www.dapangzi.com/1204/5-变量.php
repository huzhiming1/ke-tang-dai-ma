<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-04
 * Time: 16:19
 */

//变量的赋值
$a = 10;
$b = $a;//相当于复制了一份
$b = 30;
echo $a;//10
echo $b;//30


//内存地址赋值方式
echo "<hr>";
$c = 20;
$d = &$c;//
$d = 40;

echo $c."<br>";//40
echo $d."<br>";//40


$a1 = "1";//把它的内存地址传给函数

function funccc(&$var){
    global $a1;//声明为全局
    $a1 = 50;
//    $var = "2";
}
funccc($a1);
echo $a1;//50


//可变 变量 （对象）
 $abc = "abcde";
 $$abc = 10;
 var_dump($abcde);

$a = 'hello';
//$hello = 'world';
$$a='world';

