<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php
/**
 * Created by PhpStorm.
 * User: dapeng
 * Date: 2019-12-04
 * Time: 14:34
 */

$arr = ["a","b","c","d","e"];
var_dump(count($arr));

//基础循环
//数组的函数长度
for ($i =0 ;$i<count($arr);$i++){
    echo $arr[$i];
//    var_dump($arr[$i]);
}

echo "<hr> <h2>foreach循环</h2>";

// 循环    数组  as  数组中的某一项
//foreach ( array as object )
foreach ($arr as $item){
    echo($item);
}

echo "<hr> <h2>foreach循环键值对数组</h2>";
$keyArray = [
    "user" => "张三",
    "pass" => "123456"
    ];

foreach ($keyArray as $key => $value){
    echo "键=".$key."值=".$value."<br>";

    echo "键={$key}1,值={$value}2<br>";
    echo "键=\" \" $key}1,值={$value}2<br>";
}

//C:\\xampp\\htdoc\\php\\
//C:/xampp/htdoc/php/
